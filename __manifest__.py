# -*- coding: utf-8 -*-
{
    'name': "HospitalDPB",

    'description': """
        Aplicación Gestión de pacientes
    """,

    'author': "Daniel Peñarroya",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/13.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Extra Tools',
    'version': '0.1',
    'summary': 'Module for managing the Hospitals',
    'sequence': '10',
    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'data/patient.xml',
        'views/views.xml',
        'views/templates.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
        'demo/demo_metge.xml',
        'demo/demo_especialitat.xml',
        'demo/demo_test.xml',
    ],
    'installable':True,
    'application':True,
    'auto_install': False,
}
