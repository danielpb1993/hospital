# -*- coding: utf-8 -*-

from odoo import models, fields, api


# class hospital(models.Model):
#     _name = 'hospital.hospital'
#     _description = 'hospital.hospital'

#     name = fields.Char()
#     value = fields.Integer()
#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()
#
#     @api.depends('value')
#     def _value_pc(self):
#         for record in self:
#             record.value2 = float(record.value) / 100

class patient(models.Model):
    _name = "hospital.patient"
    _description = 'Pacient d\'hospital'
    _rec_name = 'nom'
    _order = "nom"

    nom = fields.Char(required=True, string='Nom')
    cognom = fields.Char(required=True, string='Cognom')
    dni = fields.Char(required=True, string='DNI')
    genere = fields.Selection([('h', 'Home'), ('d', 'Dona')], 'Gènere')
    adreca = fields.Char(required=True, string='Adreça')
    data_neixement = fields.Date(required=True, string='Data de neixement', default=fields.datetime.now())
    nacionalitat = fields.Many2one('res.country', string='Nacionalitat')

    ingres = fields.Boolean(required=True, string='Ingrès ?', default=False)
    data_ingres = fields.Date(string='Data d\'ingrès', default=fields.datetime.now())
    informacio = fields.Text(string='Informació')
    asseguranca = fields.Char(string='Assegurança')

    prova_ids = fields.One2many('hospital.hospital_dpb_test', 'pacient_id', string='prova_ids')
    doctor_rel = fields.Many2one('hospital.hospital_dpb_metge', string='doctor_rel')


class Especialitat(models.Model):
    _name = "hospital.hospital_dpb_especialitat"
    _description = 'Especialitat'
    _rec_name = 'nom'
    _order = "nom"

    nom = fields.Char(required=True, string='Nom')
    edifici = fields.Char(required=True, string='Edifici')
    metge_ids = fields.One2many('hospital.hospital_dpb_metge', 'edifici', string='metge_ids')


class Metge(models.Model):
    _name = "hospital.hospital_dpb_metge"
    _description = 'Metge'
    _rec_name = 'nom'
    _order = "nom"

    nom = fields.Char(required=True, string='Nom')
    cognom = fields.Char(required=True, string='Cognom')
    dni = fields.Char(required=True, string='DNI')
    genere = fields.Selection([('h', 'Home'), ('d', 'Dona')], 'Gènere')
    adreca = fields.Char(required=True, string='Adreça')
    anys_experiencia = fields.Integer(string="Anys d'experiència")

    especialitat_id = fields.Many2one('hospital.hospital_dpb_especialitat', ondelete='Set null', string='Especialitat')
    edifici = fields.Char(string='Edifici', related='especialitat_id.edifici')
    pacient_rel = fields.Many2many('hospital.patient', 'doctor_rel', string='pacient_rel')


class Test(models.Model):
    _name = 'hospital.hospital_dpb_test'
    _description = 'Medical Tests'
    _rec_name = 'nom'
    _order = "nom"

    nom = fields.Char(required=True, string='Nom')
    codi = fields.Selection([('Radio', 'Radiologia'), ('Extr', 'Extraccions'),('Electro', 'Electro')], 'Tipus')
    data_prova = fields.Date(string='Data prova', default=fields.datetime.now())
    pacient_id = fields.Many2one('hospital.patient', string='pacient_id')
